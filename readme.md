# yauzl-ai

yauzl async iterable version

Has the same methods as `yauzl` but promisified and proxied to `AiZipFile`.

## AiZipFile

**aiZipFile[Symbol.asyncIterator]()**

Call once to get an iterator for the zip file. Iterates through the entries.

**aiZipFile.openReadStreamReadable(entry, options)**

Version of `openReadStream` without the callback. Returns a `Readable` readable stream.
