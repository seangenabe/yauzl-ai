import { Readable, PassThrough } from "stream"

export function promiseReadableToReadable(source: Promise<Readable>): Readable {
  const ret = new PassThrough()
  ;(async () => {
    const s = await source
    s.pipe(ret)
    s.on("error", (err) => ret.emit("error", err))
  })()
  return ret
}
