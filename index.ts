import yauzl, {
  Options,
  ZipFile,
  Entry,
  RandomAccessReader,
  ZipFileOptions,
} from "yauzl"
import { promisify } from "util"
import { Readable } from "stream"
import { promiseReadableToReadable } from "./util"

export { Options, ZipFile, Entry, RandomAccessReader, ZipFileOptions }

const yOpen = promisify<string, Options, ZipFile>(yauzl.open as any)
const yFromFd = promisify<number, Options, ZipFile>(yauzl.fromFd as any)
const yFromBuffer = promisify<Buffer, Options, ZipFile>(yauzl.fromBuffer as any)
const yFromRandomAccessReader = promisify<
  RandomAccessReader,
  number,
  Options,
  ZipFile
>(yauzl.fromRandomAccessReader as any)

function makeIterable(zipFile: ZipFile): asserts zipFile is AiZipFile {
  ;(zipFile as AiZipFile)[
    Symbol.asyncIterator
  ] = async function* getAsyncIterator(this: ZipFile) {
    const error = new Promise<never>((_, reject) => zipFile.on("error", reject))
    const end = new Promise<void>((resolve) => zipFile.on("end", resolve))

    do {
      const value = new Promise<Entry>((resolve) => zipFile.once("entry", resolve))
      zipFile.readEntry()
      const entry = await Promise.race([error, end, value])
      if (entry == null) {
        return
      }
      yield entry
    } while (true)
  }.bind(zipFile)

  const ors = promisify<Entry, ZipFileOptions, Readable>(
    zipFile.openReadStream.bind(zipFile) as any
  )

  ;(zipFile as AiZipFile).openReadStreamReadable = function openReadStreamAi(
    this: ZipFile,
    entry: Entry,
    options: ZipFileOptions
  ): Readable {
    return promiseReadableToReadable(ors(entry, options))
  }.bind(zipFile)
}

export async function open(path: string, options: AiOptions = {}) {
  const zipFile = await yOpen(path, { ...options, lazyEntries: true })
  makeIterable(zipFile)
  return zipFile
}

export async function fromFd(fd: number, options: AiOptions = {}) {
  const zipFile = await yFromFd(fd, { ...options, lazyEntries: true })
  makeIterable(zipFile)
  return zipFile
}

export async function fromBuffer(buffer: Buffer, options: AiOptions = {}) {
  const zipFile = await yFromBuffer(buffer, { ...options, lazyEntries: true })
  makeIterable(zipFile)
  return zipFile
}

export async function fromRandomAccessReader(
  reader: RandomAccessReader,
  totalSize: number,
  options: AiOptions = {}
) {
  const zipFile = await yFromRandomAccessReader(reader, totalSize, {
    ...options,
    lazyEntries: true,
  })
  makeIterable(zipFile)
  return zipFile
}

export type AiOptions = Omit<Options, "lazyEntries">

export interface AiZipFile extends ZipFile, AsyncIterable<Entry> {
  openReadStreamReadable(entry: Entry, options?: ZipFileOptions): Readable
}
